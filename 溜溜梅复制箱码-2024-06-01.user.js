// ==UserScript==
// @name         溜溜梅复制箱码
// @namespace    https://codeberg.org/canqing/Tampermon-Work
// @version      2024-06-01
// @description  2024-06-01 复制按钮 点击复制
// @author       You
// @match        *.one.liuliumei.com:8011/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=liuliumei.com
// @grant        GM_setClipboard
// ==/UserScript==

(function() {
    'use strict';
    //  http://one.liuliumei.com:8011/ 账号和密码CKGLY

    let my_timer

    //my_timer = setInterval(isXiangMa, 5000);//使用自动复，只需要开启本段，屏蔽 下段 CopyLLM()

    CopyLLM();

    function CopyLLM(){
        var iframe = document.getElementById('rightFrame');// 获取iframe元素，因为是多层嵌入结果，发现关键的iframe元素就开始创建按钮

        if (iframe) { //可以用 iframe.contentWindow  检查iframe是否已加载
            console.log(iframe);
            var newButton = document.createElement("button");// 创建一个新的按钮元素
            newButton.textContent = "复制箱码";// 设置按钮的文本内容
            newButton.id = "mycopy"
            newButton.onclick = function(){rightFrame();}
            var htmlElement = document.documentElement;// 获取html元素
            var headElement = document.head;// 获取head元素
            htmlElement.insertBefore(newButton, headElement);// 将新按钮插入到head元素之前

        }

    }

    function rightFrame(){//在嵌入的rightFrame元素中提取
        const rightFrame = document.getElementById('rightFrame')
        const iframeDocument = rightFrame.contentDocument || rightFrame.contentWindow.document;
        const element = iframeDocument.getElementById('ctl00_ContentPlaceHolder1_gridView');
        if (element){
            //console.log("【element存在】");
            let tableData = element.outerText
            if (tableData.includes('箱码')) {
                clearInterval(my_timer);
                //console.log(tableData);
                GetXiangMa(tableData)
                //console.log("tableData中包含'箱码'这个文本");

            } else {
                //console.log("tableData中不包含'箱码'这个文本");
                alert("当前页面没有箱号！tableData中不包含'箱码'这个文本")
            }
        }else{alert("当前页面没有箱号！且没有element存在元素")}

    }



    //此方法是充根目录开始判断，有多个iframe 跨域,发现有箱码了就自动复制，然后停止执行脚本
    function AutoCopyXiangMa(){
        console.log("【正在判断是否有箱码】");
        const mainFrame = document.getElementById('mainFrame');
        if (mainFrame){
            //console.log("【mainFrame存在】");
            const mainFrameDocument = mainFrame.contentDocument || mainFrame.contentWindow.document;
            const rightFrame = mainFrameDocument.getElementById('rightFrame');
            if (rightFrame){
                //console.log("【rightFrame存在】");
                const iframeDocument = rightFrame.contentDocument || rightFrame.contentWindow.document;
                const element = iframeDocument.getElementById('ctl00_ContentPlaceHolder1_gridView');
                if (element){
                    //console.log("【element存在】");
                    let tableData = element.outerText
                    if (tableData.includes('箱码')) {
                        clearInterval(my_timer);
                        //console.log(tableData);
                        GetXiangMa(tableData)
                        //console.log("tableData中包含'箱码'这个文本");

                    } else {
                        //console.log("tableData中不包含'箱码'这个文本");
                    }
                }
            }
        }
    }

    function GetXiangMa(tableData){
        const regex = /(\d{12} \d{17})/g;

        let resultText = '';
        let match;

        // 使用全局标志g进行多次匹配
        while ((match = regex.exec(tableData)) !== null) {
            // match[1] 包含第一个捕获组，即14位数字
            resultText += match[1] + '\n'; // 将数字添加到结果文本中，每个数字后跟一个换行符
        }

        // 在循环结束后打印结果文本
        console.log(resultText.trim()); // 使用trim()移除可能的开头或结尾的空白字符（如换行符）
        GM_setClipboard(resultText.trim(), "text");
        alert("已复制：\n"+resultText.trim().substring(0, 120)+"\n......")
    }
    // Your code here...
})();