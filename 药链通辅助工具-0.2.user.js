// ==UserScript==
// @name         药链通辅助工具
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  try to take over the world!
// @author       Wei
// @match        https://ylt.cdhywl.cn/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=ylt.cdhywl.cn
// @run-at       document-end
// @grant        GM_setClipboard
// ==/UserScript==

(function() {
    'use strict';
    //debugger;
    var aa="";
    var oldhref;
    //var oldhref = location.href;
    let my_timer;

    my_timer = setInterval(function () {
        if (location.href != oldhref) {
            console.log("监听到地址变化,启动【star】!",location.href);
            oldhref = location.href;
            star();
        }
    }, 5000);/*停止定时器 clearInterval(my_timer); */

    function star(){
        if (location.href.match(".*/#/storage/record/info\\?code=CRK.*") != null){
            //console.log("【复制入库明细】")
            copyinfo();
        }
        else if (location.href.match(".*/#/storage/handle") != null){
            //console.log("【复制出库明细】")
            CopyChuKu();
        }else if (location.href.match(".*/#/orders/search/info\\?code=.*") != null){
            //console.log("【订单详情】")
            CopyDingDan();
        }else if (location.href == "https://ylt.cdhywl.cn/pt/#/orders/search"){
            //console.log("【快捷搜索】")
            FastSearch();
        }else{
            console.log("当前无需执行代码！")
        }
    }


    function copyinfo(){
        if(document.querySelector("button#mycopy")){return}
        console.log("准备创建【复制 入库明细】按钮");
        var mydiv = document.createElement('div');
        mydiv.innerHTML = `<p id="my_add_dizhi" style="color:black;font-size:16px"> 下表品名规格 <button id="mycopy">点此复制</button> </p>`;
        document.querySelector(".ylt-seamless.el-row").before(mydiv);
        document.querySelector("button#mycopy").onclick = function(){getdatainfo();}

    }

    function getdatainfo(){//获取入库数据
        var table = document.querySelector("div.el-table__body-wrapper.is-scrolling-none > table > tbody").innerText
        table = table.replace(/\n\t\n/g,"\t");
        table = table.replace(/\n\n\n/g,"\n");;
        GM_setClipboard(table, "text");//复制、将剪贴板的文本设置为指定的值
        alert('已复制：' + table);
    }


    function CopyChuKu(){
        if(document.querySelector("button#mycopy")){return}
        console.log("准备创建【复制 出库明细】按钮");
        let bb = document.querySelector("span.tags-view-item.router-link-exact-active.router-link-active.active")
        if (bb!=null){
            //bb.innerText="【云仓处理】"
            var mydiv = document.createElement('div');
            mydiv.innerHTML = `<p id="my_add_dizhi" style="color:black;font-size:16px"> 【出库明细】 <button id="mycopy">【点此复制】</button> </p>`;
            document.querySelector(".el-dialog__title").before(mydiv);
            document.querySelector("button#mycopy").onclick = function(){getdataChuKu();}
        }else{oldhref=""}
        //clearInterval(my_timer);
    }

    function getdataChuKu(){//获取出库数据

        let table = document.querySelector(".ylt-table-minor")
        let text = table.innerText

        //text = '阿莫西林胶囊\n\t\n0.25g*24粒\n\t\n230601\n\t\n1\n\t\n205-17\n\n\n咳特灵胶囊\n\t\n15*2\n\t\n230408\n\t\n1\n\t\n122-14'

        text = text.replace(/\n\t\n/g,"\t");
        text = text.replace(/\n\n\n/g,"\n");
        text = text.replace(/\n\n/g,"\n");
        if (text.includes("库位")) {
            text = text.substring(16) //只要余货库位后面的文本
        }

        text = text.trim() //去首尾空字符

        GM_setClipboard(text, "text");//复制、将剪贴板的文本设置为指定的值
        alert('已复制：' + text);
    }
    function CopyDingDan(){
        if(document.querySelector("button#mycopy")){return}
        console.log("准备创建复制【订单详情】按钮");
        let bb = document.querySelector("button.el-button.ylt-mini-button.el-button--primary.el-button--medium.is-plain")
        if (bb =null){oldhref="";return}//可能还没有加载完成，继续检测
        var mydiv = document.createElement('div');
        mydiv.innerHTML = `<p id="my_add_dizhi" ><button id="mycopy">复</button> </p>`;
        document.querySelector("button.el-button.ylt-mini-button.el-button--primary.el-button--medium.is-plain").before(mydiv);
        document.querySelector("button#mycopy").onclick = function(){GetDingDan();}
    }

    function GetDingDan(){//获取订单详情

        let table = document.querySelector(".ylt-table-minor")
        let text = table.innerText

        text = text.replace(/\n\t\n/g,"\t");
        text = text.replace(/\n\n\n/g,"\n");
        text = text.replace(/\n\n/g,"\n");
        if (text.includes("总价值")) {
            text = text.substring(60) //只要总价值后面的文本
        }

        text = text.trim() //去首尾空字符

        GM_setClipboard(text, "text");//复制、将剪贴板的文本设置为指定的值
        alert('已复制：' + text);
    }

    function FastSearch(){//快速搜索
        if(document.querySelector("button#mycopy1")!=null){return}
        console.log("准备创建【快捷搜索】按钮");
        var mydiv = document.createElement('div');
        mydiv.innerHTML = `<p id="my_add_dizhi" ">
        <button id="mycopy1">乐千团</button>
        <button id="mycopy2">九寨沟</button>
        <button id="mycopy3">得诺</button>
        <button id="mycopy4">彩虹</button>
        </p>`;
        document.querySelector(".el-breadcrumb.app-breadcrumb.breadcrumb-container").after(mydiv);
        document.querySelector("button#mycopy1").onclick = function(){FastSearchonclick("乐千团");}
        document.querySelector("button#mycopy2").onclick = function(){FastSearchonclick("九寨沟");}
        document.querySelector("button#mycopy3").onclick = function(){FastSearchonclick("得诺");}
        document.querySelector("button#mycopy4").onclick = function(){FastSearchonclick("彩虹");}
    }

    function FastSearchonclick(aa){//快速搜索

        let event = document.createEvent('HTMLEvents');
        event.initEvent("input", true, true);
        let account = document.querySelector("#ylt-scrollbar > div.el-scrollbar__wrap > div > div > div.ylt-search-bar.ylt-block.el-row > form > div:nth-child(2) > div:nth-child(2) > div > div > input")
        account.value = aa;
        account.dispatchEvent(event);

        document.querySelector("#ylt-scrollbar > div.el-scrollbar__wrap > div > div > div.ylt-search-bar.ylt-block.el-row > form > div:nth-child(2) > button").click()

    }
    // Your code here...
})();